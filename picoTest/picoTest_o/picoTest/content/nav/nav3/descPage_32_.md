---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav3
desc: true
type: textframe
assetName: 3-1-2.jpg
orderNr: 2
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Vēstures avoti un novadpētnieku darbi liecina, ka Varakļānos gandrīz uz katras ielas savulaik ir bijušas ebreju dzīvojamās mājas un veikali. Ebreji, dzīvojot svešā kultūrtelpā un ievērojot noteiktus kopienas veidošanas nosacījumus, Varakļānos izveidoja jauktu dzīves vietu, kura tika piemērota iedzīvotāju personiskajām un individuālajām vajadzībām. Ebreju mājas simboliskais spēks ir ietverts tās iemītnieku un sabiedrības mijiedarbībā, bet vietas izvēlē apliecina zināmu distancētību. Ebreju mājā saimnieks tradicionāli ir vīrietis, tādējādi akceptējot viņu ģimenēm raksturīgo patriarhālo modeli, tomēr mājas būtībā ir saskatāmas arī sievišķīgi mātišķas iezīmes. Ebreju „mājas” izjūta, iespējams, bijusi saistīta ar vidi, kam raksturīgs iedalījums dažādos līmeņos. Kā, piemēram, māja, iela, rajons, u.c.).<br><br>Daudzas no ebreju apdzīvotajām ēkām ir vairākkārt pārpirktas un pārbūvētas, un tas mūsdienās apgrūtina šo ēku apzināšanu un atrašanu. Ebreju īpaši iecienīta ir bijusi Rīgas iela, kuras centrālajā daļā māju apbūves blīvums ir bijis ļoti neliels. Rīgas ielai bijusi raksturīga frontāla apbūve: kā liecina pirms Otrā pasaules kara uzņemtie foto attēli, tad mājas ir atradušās gandrīz viena otrai blakus. Mājas vietas izvēlē izšķirošais aspekts bija saistīts ar iemītnieku kopīgo vēsturi un vienojošajiem apstākļiem, kas veidoja viņu piederības izjūtu vienai kopienai. Ebreji pārsvarā ir dzīvojuši vienstāva koka ēkās, tomēr ir bijušas arī dažas divstāvu koku ēkas. Ēku lielums, funkcionalitāte un citi vizuāli uztverami elementi raksturo bijušo īpašnieku patību, kas kontekstā ar sociāli kulturālajiem sakariem ilustrē ebreju identitāti. To akcentē arī Baiba Bela-Krūmiņa, izsakoties, ka identitāte ir patības izjūta, kas veidojas, nošķirot sevi no vecākiem un ģimenes un ieņemot vietu sabiedrībā. Identitāte turpina veidoties sociālajās attiecībās (Bela-Krūmiņa, 2001, 76).
</div>


<div class="underText"></div>

