---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav3
desc: true
type: textframe
assetName: 3-1-18.jpg
orderNr: 18
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
<b>Atmiņu stāsts</b><br><br>Antoņina Vecstaudža stāsta, ka ebreju mājas bijušas būvētas viena blakus otrai. Katrā no šīm mājām esot bijis arī veikals vai kāda cita ebreju tirgotava. Viena no lielākajām ēkām, kura piederējusi ebrejiem, ir bijusi t.s. „Patērētāju biedrības” (padomju laika apzīmējums, jo veikalu vairākums tajā laikā piederēja patērētāju biedrībām) ēka. Mūsdienās šajā ēkā arī atrodas vairāki veikali. Antoņina Vecstaudža stāsta, ka viņa „Patērētāju biedrībā” esot nostrādājusi vairākus gadus, un tur esot strādājuši arī ebreju tautības cilvēki. Tāpat viņa atceras, ka ēkā, kas atrodas pretī Svētās Marijas Romas Katoļu baznīcai, arī kādreiz esot atradies ebreju veikals. Šajā veikalā savulaik dažāda vecuma cilvēki varējuši iegādāties apģērbu, atbilstoši savai gaumei. Šinī ēka ebreji ne tikai tirgojušies, bet arī dzīvojuši. Tā starp ebrejiem esot bijusi ierasta prakse, ka veikali atradušies dzīvojamās mājas pirmajā stāvā (Vecstaudža, 2013, 3).<br><br>Lai arī bija pagājis ievērojams laiks kopš ebreji vairs tur nedzīvoja un netirgojās, tomēr vēl 20. gadsimta 90. gados, kad šajā ēkā atradies pārtikas veikals, vecākās paaudzes varakļānieši šo ēku esot dēvējuši par „Žida veikalu” (Vecstaudža, 2013, 3), tādējādi uzsverot šīs ēkas vēsturisko piederību. Stāsts apliecina, ka māju lomu un to nozīmīgumu cilvēki nereti pamana tikai tad, kad ir izmainījies ēkas vizuālais izskats un tās pamatfunkcijas.Ebreju</div>


<div class="underText">Ebreju friziera dzīvojamās ēkas foto. 2014. gads. Rīgas iela. Autora (Strods, 2014, KS 4)</div>

