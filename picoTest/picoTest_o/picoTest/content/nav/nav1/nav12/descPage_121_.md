---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage03.twig
Parrent: nav12
desc: true
type: videos
assetName: Jewish-Life.mp4
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div hidden class="cardName">Lapas apraksts</div>
<div hidden class="underText">Paraksts</div>

