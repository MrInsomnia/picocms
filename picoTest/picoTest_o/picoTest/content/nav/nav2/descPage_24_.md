---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-4.jpg
orderNr: 4
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Ebreju sešgadīgās pamatskolas projekta plānojumā redzams, ka blakus skolas ēkai bija ieplānots skolas sporta un vingrošanas laukums, kā arī botāniskais sakņu dārzs (LVVA, 6343. f., 1. a., 7. l., 140. l., 4. lp.) (9. pielikums). Diemžēl trūkst informācijas, vai dažādu skolas projekta izbūves izmaiņu rezultātā ir notikusi reāla šo paredzēto objektu izbūve. Iepriekš nosauktie objekti arī norāda uz projekta apjomīgumu un plānojuma nopietnību.
</div>

<div class="underText">Varakļānu 6. gadīgās ebreju pamatskolas celtniecība, 2010, http://regionalistika.lv/lv/exposition/ varaklani)</div>

