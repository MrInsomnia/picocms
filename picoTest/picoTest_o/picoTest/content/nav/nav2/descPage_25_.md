---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-5.jpg
orderNr: 5
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Skolas būvniecības laikā nemitīgi ir notikušas projekta izmaksu palielināšanas, atkāpes no būvniecības plāna, kā arī radušās dažādas citas problēmas.<br><br>Tas spilgti atspoguļojas būvinspektora A. Bučinska 1926. gada 21. septembra ziņojumā, kurā viņš min atkāpes no būvniecības plāna un nekvalitatīvu materiālu izmantošanu celtniecības gaitā (LVVA, 6343. f., 1. a., 7. l., 140. l., 24 lp.). Neskatoties uz dažādiem šķēršļiem skolas būvniecības gaitā, 1929.g. 5. janvārī ieplānota skolas svinīga pieņemšana. Par to ziņo Varakļānu pilsētas priekšsēdētājs P. Švarcs (LVVA, 6343. f., 1. a., 7. l., 140. l., 52. lp.)
</div>

<div class="underText"></div>

