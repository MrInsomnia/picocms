---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-1.jpg
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName"><b>Ebreju pamatskolas ēkas</b><br><br>Ebreju bērni līdz savas skolas jaunās ēkas celtniecībai un arī tās pastāvēšanas laikā mācījušies arī citās Varakļānu skolās. Piemēram, Jūlijs Počs atceras, ka Varakļānu otrajā 6 - gadīgajā pamatskolā arī mācījušies ebreju tautības bērni, kaut arī viņiem izglītība pilnībā tika nodrošinātā ebreju sešgadīgajā pamatskolā. Latviešu un ebreju bērni savā starpā esot mierīgi sadzīvojusi, un problēmu neesot bijis (Počs, 2007, 133). Sākumā ebreju bērniem mācības notikušas nelielā koka ēkā. Fotogrāfijai, kurā redzami šīs skolas pedagogi, labajā augšējā stūrī pievienotā informācija (Varakļānu ebreju skolas ēkas foto, 2010, http://regionalistika.lv/lv/exposition/varaklani) liecina, ka tie ir bijuši L. Joffe, S. Gabaji, I. Maslovskaja, B. Šapiro u.c</div>

<div class="underText">Varakļānu ebreju pamatskolas ēkas foto (Varakļānu ebreju skolas ēkas foto, 2010, http://regionalistika.lv/lv/exposition/varaklani)</div>

