---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav45
desc: true
type: textframe
assetName: 4-5-2.jpg
orderNr: 2
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">На отдыхе во дворе после обеда.Вероятно 1933 или 34-ый год. В нижнем ряду слева-дядя Шлейме, рфдом выше над ним моя мама Эстер (я еще не родился). В кресле-брат моей мамы Давид Шварц вместе с своей женой Полей. В 1935 году они уехали в Палестину. В последнем ряду-стоит- второй слева мой дедушка, отец моей мамы- Пинхус-Бер Шварц, умер внезапно, говорят-от разрыва сердца- в 1934 году на железно дорожном вокзале в Двинске и там же похоронен. Моя мама рассказывала, (я ре нашел других источников), что он был мэром города (может быть в архиве в Вараклянах или Мадоне имеется такая информация. Если Вы можете в этом помочь, буду премного благодарен. Ведь я назван в его память). Других на снимке не знаю</div>

<div class="underText"><span style="font-size: 18px;">Ebreju grupa pēc pusdienām atpūšas pagalmā. Pirmajā rindā no apakšas pirmais Šleime, virs viņa rindu augstāk Estere Hnoha. Krēslā sēž Dāvids Švarcs ar sievu Polju. 1935. gadā viņi aizbraukuši uz Palestīnu. Pēdējā rindā otrais no kreisās puses stāv Pinhus - Bērs Švarcs, pēkšņi nomiris, bijis pilsētas mērs</span> </div>

