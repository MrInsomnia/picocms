---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav45
desc: true
type: textframe
assetName: 4-5-4.jpg
orderNr: 4
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Довоенный кусочек ул. Ригас в Вараклянах. На снимке я узнаю только двоих. Мой папа Хнох Гирш , второй справа, в зимней шапке и полушубке, руки в карманах и дядя Шлейме Хнох , крайний слева, сидит на камнях. Над ним под крышей вывеска на магазине братьев Хнох- лошадиная сбруя (они были шорниками и продавали своими руками сделанный товар) После прихода сов. власти магазин национализировали , а папу назначили управляющим на зарплате. Магазин и все остальные деревянные постройки , которые на снимке, сгорели во время войны.</div>

<div class="underText"><span style="font-size: 18px;">Rīgas iela pirms II pasaules kara. Brāļu Knohu ziglietu veikals ( attēlā ar izkārtni) </span> </div>

