//This piece of art is copy&pasted from previous projects, so please don't judge me by the lack of time to rewrite it..

var cardX = 850;
var cardY = 140;
var cardH = 750;
var cardW = 1000;
var timerInterval = 40;
var difOpen = 0.07;
var textPadding = 10;
//max iespējamie slaidi
var startZ = 100;

//Nevajag mainīt
var open = 0;
var maxindex = 0;
var getTo = 0;
var ongoing = false;
var timer;
var cards;
var texts;
var utexts;


function MOD(num) {
    if (num >= 0) {
        return num;
    } else {
        return num * -1;
    }
}

function initialize(element, index) {
    //  element = document.getElementById();
    element.setAttribute('index', index);
    if (cards.length > 1) {
        element.innerHTML = ' <div onclick="prewCard()" class="prew"> </div> '
        element.innerHTML += ' <div onclick="nextCard()" class="next"> </div>';
    }
    var currStyle = 'position:fixed; display:block;';
    //  currStyle += '-webkit-filter: blur(' + index + 'px); ';
    currStyle += 'top:' + (cardY) + 'px; ';
    currStyle += 'left:' + (cardX) + 'px; ';
    currStyle += 'width:' + cardW + 'px; ';
    currStyle += 'height:' + (cardH) + 'px; ';
    // currStyle += 'box-shadow: 1px 1px 2px #555;';
    if (index == open) {
        currStyle += 'z-index:' + startZ + ';';
    } else {
        currStyle += 'z-index:' + (startZ - MOD(index - open)) + '; ';
    }
    var image = element.getAttribute('image');
    currStyle += 'background-image: url("images/' + image + '"); ';
    element.setAttribute("style", currStyle);
    maxindex = index;
    var buttons = document.getElementsByClassName('prew');
    for (index = 0; index < buttons.length; ++index) {
        buttons[index].style.display = "none";
    }

}

function initializeText(element, index) {
    //  element = document.getElementById();
    element.setAttribute('index', index);
    var currStyle = 'position:fixed; ';
    if (index == open) {
        currStyle += 'opacity:' + 1 + ';';
    } else {
        currStyle += 'opacity:' + 0 + ';';
    }
    element.setAttribute("style", currStyle);
}


function nextCard() {
    if (ongoing) {
        return;
    }
    if (open + 1 >= maxindex) {
        var buttons = document.getElementsByClassName('next');
        for (index = 0; index < buttons.length; ++index) {
            buttons[index].style.display = "none";
        }
    }
    var buttons = document.getElementsByClassName('prew');
    for (index = 0; index < buttons.length; ++index) {
        buttons[index].style.display = "block";
    }
    if (open < maxindex) {
        ongoing = true;
        getTo = open + 1;
        timer = setInterval(drawscreenN, timerInterval);
    }
}

function prewCard() {
    if (ongoing) {
        return;
    }
    if (open - 1 <= 0) {
        var buttons = document.getElementsByClassName('prew');
        for (index = 0; index < buttons.length; ++index) {
            buttons[index].style.display = "none";
        }
    }
    var buttons = document.getElementsByClassName('next');
    for (index = 0; index < buttons.length; ++index) {
        buttons[index].style.display = "block";
    }
    if (open > 0) {
        ongoing = true;
        getTo = open - 1;
        timer = setInterval(drawscreenP, timerInterval);
    }
}

function drawscreenP() {
    open -= difOpen;
    if (open < getTo) {
        open = getTo;
        window.clearInterval(timer);
        ongoing = false;
    }
    for (index = 0; index < cards.length; ++index) {
        drawCard(cards[index], index);
    }
    for (index = 0; index < texts.length; ++index) {
        drawText(texts[index], index);
    }

    for (index = 0; index < utexts.length; ++index) {
        drawText(utexts[index], index);
    }

}

function drawscreenN() {
    open += difOpen;
    if (open > getTo) {
        open = getTo;
        window.clearInterval(timer);
        ongoing = false;
    }
    for (index = 0; index < cards.length; ++index) {
        drawCard(cards[index], index);
    }
    for (index = 0; index < cards.length; ++index) {
        drawText(texts[index], index);
    }
    for (index = 0; index < utexts.length; ++index) {
        drawText(utexts[index], index);
    }
}

function drawText(element, index) {
    element.setAttribute('index', index);
    var currStyle = 'position:fixed;';
    if (MOD(index - open) >= 1) {
        currStyle += 'opacity:' + 0 + ';';
    } else {
        if (index - open > 0) {
            currStyle += 'opacity:' + (1 - MOD(open - index)) + ';';
        } else {
            currStyle += 'opacity:' + (1 - MOD(index - open)) + ';';
        }
    }
    element.setAttribute("style", currStyle);
}


function drawCard(element, index) {
    var currStyle = 'position:fixed; ';
    var image = element.getAttribute('image');
    currStyle += 'background-image: url("images/' + image + '"); ';
    currStyle += 'top:' + (cardY) + 'px; ';
    currStyle += 'left:' + (cardX) + 'px; ';
    currStyle += 'width:' + cardW + 'px; ';
    currStyle += 'height:' + (cardH) + 'px; ';
    if (MOD(index - open) >= 1) {
        currStyle += 'opacity:' + 0 + ';';
    } else {
        if (index - open > 0) {
            currStyle += 'opacity:' + (1 - MOD(open - index)) + ';';
        } else {
            currStyle += 'opacity:' + (1 - MOD(index - open)) + ';';
        }
    }
    element.setAttribute("style", currStyle);
}

window.onload = function () {
    cards = document.getElementsByClassName("card");
    texts = document.getElementsByClassName("cardName");
    utexts = document.getElementsByClassName("underText");
    for (index = 0; index < cards.length; ++index) {
        initialize(cards[index], index);
    }
    for (index = 0; index < texts.length; ++index) {
        initializeText(texts[index], index);
    }
    for (index = 0; index < utexts.length; ++index) {
        initializeText(utexts[index], index);
    }

    document.getElementsByTagName('body')[0].addEventListener("click", function () {
        resetTimer();
    });

};


var closer;
var sec = 120;
closer = setTimeout(resetPage, sec * 1000);
function resetPage() {
    window.location.href = '/?reset=true';
}

function resetTimer() {
    window.clearTimeout(closer);
    closer = setTimeout(resetPage, sec * 1000);
}