---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav3
desc: true
type: textframe
assetName: 3-1-16.jpg
orderNr: 16
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Kā liecina fotogrāfija, tad starp koka ēkām ir redzama arī kāda ķieģeļu ēka, kas pirms Otrā pasaules kara ir bijis samēra reti. Ebreju mājās raksturīga bija publisko telpu (veikala, darbnīcas) atdalīšana no privātajām – ģimenes apdzīvotajām telpām. Šāds māju koncepts bija izplatīts vidusšķirā, kurā publiskā telpa asociējās ar ražošanu, tirdzniecību, pilsētas dzīvi, tāpat kā ar briesmām, bezpersoniskumu, patībai nepieņemamu.  rēji mājas reprezentēja ģimenes sociālo statusu, tās morālās vērtības, un īpašnieku gaumi. Iepriekš minētie elementi apliecina identitāti, kas kā cilvēka patības daļa atspoguļo mainīgās sociālās lomas, un arī to dažādos statusus.
</div>


<div class="underText">Rīgas iela 1926.gads</div>

