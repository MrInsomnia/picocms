---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-2.jpg
orderNr: 2
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Varakļānu ebrejs Pauls Tessels savās atmiņās atceras, ka B. Šapiro ir bijusi viņa ģeogrāfijas skolotāja (Tessels, 2000, 6). Diemžēl pagaidām vēl nav izdevies noskaidrot šīs ebreju skolas ēkas precīzu atrašanās vietu. Tomēr literatūrā ir rastas norādes, ka Varakļānu ebreji ļoti lepojušies ar savu skolu tās izcelsmes dēļ: 1918. gadā Varakļānus esot apmeklējis pirmais Latvijas valsts prezidents Jānis Čakste, kurš sarunā ar tā laika Varakļānu pilsētas mēru esot apsolījis piešķirt ebrejiem zemi savas skolas būvniecībai. Pēc pāris gadiem skola esot uzcelta par valsts un ebreju kopienas līdzekļiem (Рочко, 2003, 110).</div>

<div class="underText">Skolēnu klase. Varakļānu ebreju pamatskolā. Varakļāni. 1922. gads(Киль, 2007, 18)</div>

