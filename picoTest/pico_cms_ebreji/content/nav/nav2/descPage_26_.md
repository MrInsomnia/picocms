---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-6.jpg
orderNr: 6
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Kā redzams fotogrāfijā, tad ebreju pamatskola ir bijusi liela izmēra divstāvu kokā ēka. Pamatskola sākusi darboties 20. gadsimta 30 gados, par skolotājiem strādājuši A. Nāburgs, D. Dimants, I. Lifšics u c., skolas pārzinis bijis Š. Baskovičs. Jaunā ebreju skolas ēka atradusies tagadējo Kosmonautu un Rēzeknes ielu krustojumā (Počs, 2007, 134). Šajā skolā mācības notikušas jidišā, skolēni apguvuši arī Talmudu.<br>L. Joffe bijis jaunuzceltās Ebreju sešgadīgās pamatskolas direktors. Skolā ebreju bērni apguva latviešu valodu, ivritu, jidišu, ebreju vēsturi un literatūru. Arī fizkultūra, dziedāšana un mājturība bijusi iekļauta mācību programmā. Puišiem un meitenēm mājturība tikusi pasniegta atsevišķi. Mājturību meitenēm pasniegusi skolotāja Melameda, meitenes esot adījušas un šuvušas nodarbību laikā, savukārt puiši nodarbojušies ar koka darbiem. Matemātiku un fizkultūru esot pasniedzis pasniedzējs Kaans, kurš braucis no Viļāniem uz Varakļāniem. Talmudu esot pasniedzis rabīns Leizers Grodskis. Vienīgais latviešu tautības skolotājs bijis Nāburgs, viņš ebreju bērniem esot mācījis latviešu valodu. Visi ebreju bērni esot mācējuši latviešu valodu, taču savā starpā viņi esot sarunājušies jidišā. Daudzi ebreju bērni esot pārgājuši mācīties uz latviešu skolām. Tas izskaidrojams ar to, ka pēc sešgadīgās latviešu skolu beigšanas daudzi no viņiem stājās ģimnāzijā, kur pēc latviešu skolu beigšanas bija nepieciešams kārtot tikai četrus eksāmenus. Turpretī beidzot ebreju skolu, nācās kārtot sešus eksāmenus (Рочко, 2003, 110 – 111).
</div>

<div class="underText">Varakļānu 6-gadīgās ebreju pamatskolas foto. Pabeigta ēka, 2010, http://regionalistika.lv/lv/exposition/varaklani)</div>

