---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-3.jpg
orderNr: 3
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Latvijas Valsts vēstures arhīva materiālos atrodamā informācija liecina, ka jaunās 6-gadīgās pamatskolas pamatakmens ielikts 1925. gada 17. augustā.<br>Uz šo svinīgo notikumu tikuši aicināti Iekšlietu ministrijas būvaldes pārstāvji (LVVA, 6343. f., 1. a., 7. l., 140. l., 22 lp.).</div>

<div class="underText">Situācijas plāns Varakļānu ebreju 6-dīgās pamatskolas novietošanai Varakļānu miestā Rēzeknes apriņķī (LVVA, 6343. f., 1. a., 7. l., 140. l., 4. lp.)</div>

