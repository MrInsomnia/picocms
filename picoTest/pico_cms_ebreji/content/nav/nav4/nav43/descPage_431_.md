---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav43
desc: true
type: textframe
assetName: 4-3-1.jpg
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Diemžēl nav saglabājušies nekādi dokumenti vai foto no priekškara laikiem. Javiča vecāki, Nohums un Nehama Javiči, (skanēs jocīgi) visu mūžu strādāja uz laukiem. Tēvs pat savā ģimenē tika dēvēts "ebrejs - kolhoznieks"! Pirms kara viņi apsaimniekoja jaunsaimniecību (14 ha) Oženiekos, (Javiču tuvākie kaimiņi bija Kristapi) kas piedereja viņa mātei Rozai (dzīvoja Rēzeknē, kur arī tika nošauta kopā ar vecāko meitu, tās vīru un 2 bērniem). Tēvs Javičus aizveda uz Krievijas aizmuguri un pats tika iesaukts Sarkanajā armijā, kur kopā ar saviem 2 brāļiem Samuilu un Ilju karoja 43. Latviešu strēlnieku divīzijā. Pēc kara tēvs atgriezās uz Varakļāniem un caur tiesu atdabūja sava brālēna saimniecību Puntūžu sādžā (8 ha), jo Javiču māja Oženiekos bija nodegusi. ,br>Un tad nāca izvešanas. <br>Giršam Javičam bija 12 gadu. Tika izvesti ļoti daudzi tēva draugi, kas viņam palīdzēja nostāties uz kājām, jo viņam nebija ne zirgu, ne sēklas, lai sāktu audzēt iztiku ģimenei. Javiča tēvs pieminēja Pēteri Briedi. Pec tam nāca kolhozu laiki - "Gaišais ceļs". Javiču ģimene strādāja kā vergi, bija jaravē koksagizs. Nezāles auga labāk par kultūru! Novākto ražu sapūdēja, Stirnienes stacijā. Javiča tēvs pat bija ievēlēts par kolhoza "Gaišais ceļs" valdes priekšsēdētāju. <br>
Patreiz Giršs Javičs dzīvo ASV, māsa Leja dzīvo Izraēlā, kur arī abi vecāki atdusas.</div>
<div class="underText">2012. gada 5.augustā Varakļānos (Varakļānu vidusskolas 1955. gada izlaidums). Giršs Javičs pa kreisi un Kozulis Staņislavs pa labi.</div>

