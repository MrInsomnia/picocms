---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage02.twig
Parrent: nav51
desc: true
type: gallery
assetName: Holokausta_upuri_21.jpg
orderNr: 21
---

<div class="way">Ekrāna nosaukums</div>
<!-- <div class="header" style="color:#fff;"></div> -->

<div hidden class="cardName">Lapas apraksts</div>
<div class="underText">Ebreji - varakļānieši (holokausta upuri)</div>

