---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-7.jpg
orderNr: 7
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Arī Pauls Tessels savās atmiņās raksta, ka četrpadsmit gadu vecumā beidzis ebreju 6-gadīgo pamatskolu un tālāk turpinājis savas skolas gaitas Varakļānu ģimnāzijā (Tessels, 2000,7). Tāpat Pauls Tessels atceras, ka ģimnāzijā strādājis skolotājs Veidemanis, kurš pasniedzis latīņu valodu. Pauls Tessels raksturoja viņu kā ļoti inteliģentu un kulturālu vīrieti, kurš tekoši runājis latīņu valodā (Tessels, 2000, 7). Arī Annas Sondores veidotajā Varakļānu vidusskolas skolotāju sarakstā atrodama informācija par pasniedzēju Paulu Veidemani, kurš strādājis Varakļānu Ģimnāzijā no 1924. līdz 1933. gadam par latīņu valodas skolotāju (Sondore, 2003, 134). Savukārt Blūma Dimante atceras, ka viņas māsa, pēc ebreju 6-dīgās pamatskolas beigšanas, iestājusies Varakļānu ģimnāzijā, kura atradās tagadējā Varakļānu pils ēkā (Dimante, 2014, 2). Tādējādi tiek apstiprināta informācija, ka pēc 6-dīgās ebreju pamatskolas beigšanas daudzi ebreju bērni turpinājuši mācības Varakļānu ģimnāzijā.<br> 
Savukārt J. Staņko 1937. gada izveidotajā foto albumā „Rēzeknes apriņķa skolas” atrodams fotoattēls, kurā redzami gan Varakļānu ebreju sešgadīgās pamatskolas skolotāju grupa, gan skolēni, gan arī pati Varakļānu ebreju 6-gadīgās pamatskolas ēka (Staņko, 1937, 45).
<br>Skolas ēkas liktenis ir bijis traģisks: dažādos avotos ir norādīts, ka tā neilgi pēc Otrā pasaules kara tā ir nodegusi (Piļpuks, 2002, 103), savukārt Melera Mejera darbā „Mūsu atmiņas vieta” norādīts, ka pamatskola nodegusi Otrā pasaules kara laikā (Meйер, 2010, 85). Ticamāka ir novadpētnieka Artūra Garanča versija, ka tas noticis 1947. gadā (Počs, 2007, 134). Šo faktu netieši apstiprina arī A. Strode. Viņa savās atmiņās, konkrētu gadu gan neminot, norāda, ka viņas vīrs arī esot minējis, ka skola esot nodegusi pēc kara (Strode, 2013, 4).
</div>

<div class="underText"></div>

