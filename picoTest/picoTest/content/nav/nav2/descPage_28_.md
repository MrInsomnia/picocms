---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav2
desc: true
type: textframe
assetName: 2-1-8.jpg
orderNr: 8
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">
Hipotētiski var pieņemt, ka ugunsgrēka iemesls varētu būt skolas būvniecības laikā pieļautās neprecizitātes. Uz šādu secinājumu vedina būvinspektora A. Bučinska ziņojumsBūvniecības pārvaldei par skolas ēkas apsekošanu vēl 1933. gada 21. janvārī. Ziņojumā A. Bučinskis norāda uz sanitāro normu problēmām ēkā, tomēr būtiskākās nepilnības pēc viņa atzinuma saistītas ar jumta koka konstrukciju tuvo atrašanos apkures skursteņiem (LVVA, 6343. f., 1. a., 7. l., 140. l., 64 lp)<br>
Lai arī minētais materiāls tika iesniegts Varakļānu pilsētas valdei, nekādu turpmāko atbilžu un rīkojumu, kas liecinātu par šo problēmjautājumu risināšanu nesekoja.<br>Skolēnu skaits ebreju sešgadīgas pamatskolas klasēs ir bijis liels: piemēram, Sara Kila, aprakstot savu vecāku skolas gaitas, saka, ka viņas mātes un tēva klasē mācījušies apmēram 30 bērnu, kuri bijuši apmēram 10 – 11 gadus veci. Skolā esot bijusi samērā liberāla, piemēram,meitenes un puiši mācījušies kopā, nevis atsevišķi (Киль, 2007, 15 – 18). Tāpat Sāra atmiņās apraksta savas skolas gaitas Varakļānu ģimnāzijā. Ģimnāzijā ebreju bērni turpināja mācīties pēc savas Ebreju sešgadīgās pamatskolas beigšanas un citu apkārtējo skolu absolvēšanas (Киль, 2007, 51 – 56). Interesants ir fakts, ka Sāras ģimenē runājuši divās valodās: pirmā, kā jau ierasts no Polijas Latgalē ienākušajiem ebrejiem, bija jidiša valoda. Tomēr Sāras ģimenes locekļi labi pārvaldījuši latviešu valodu, daži – arī vācu valodu. Krieviski gan nemācējuši sarunāties, jo uzskatījuši to par nevajadzīgu (Киль, 2007, 12). Tas pierāda, ka ebreji, bijuši lojāli attiecībā pret Latvijas valsti un patriotiski noskaņoti. Ebrejiem piemīt šī vienreizējā spēja jebkurā svešā teritorijā, kur viņi apmetas, kļūt par šīs zemes un valsts patriotiem reizēm pat vairāk nekā vietējiem iedzīvotājiem. Tomēr ebreji jebkurā vietā saglabā arī savu nacionālo identitāti.<br>Jau minētās S. Kiļas atmiņas liecina, ka viņa, būdama bērns, esot gājusi pulciņā, kur bērni esot dziedājuši, piedalījušies dažādās rotaļās. Šī informācija saskan ar Josifa Ročko aprakstīto ebreju bērnudārzu (Kindergarten), kas darbojies Varakļānos 20. – 30. gados. Bērnudārzs atradies Joffes namā. Diemžēl konkrētas informācijas par šo namu nav. Bērni šeit ne tikai rotaļājušies, bet arī gatavojušies uzsākt skolas gaitas. Grupiņā bijuši apmēram no 10 – 15 bērniem. Ar bērniem darbojās skolotāja Ida Joffe, kura bija arī šī bērnudārza vadītāja (Рочко, 2003, 110). Var secināt, ka Varakļānu iedzīvotāju un pašu ebreju tautas pārstāvju atmiņas, kas pārmantotas no paaudzes paaudzē, sniedz liecības par iedzīvotāju bērnību, darba gaitām un īpašumiem pilsētvidē.
</div>

<div class="underText">A. Bučinska ziņojums Iekšlietu ministrijas būvvaldei par situāciju Varakļānu ebreju 6-dīgajā pamatskolā 1933. gada. 10. janvārī (LVVA, 6343. f., 1. a., 7. l., 140. l., 64 lp.)</div>

