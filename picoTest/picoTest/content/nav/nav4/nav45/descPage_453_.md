---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav45
desc: true
type: textframe
assetName: 4-5-3.jpg
orderNr: 3
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Вараклянская еврейская молодежь в послевоенные годы (начало шестидесятых) на еврейском ладбище в Вараклянах в День памяти убитых евреев Вараклян (4 августа 1941 г.). Слева направо мой брат Лейб Хнох (теперь Арие Ханох) -он был в нашей группе у Вас, вторая девочка слева-Рая Зёмина, умерла после репатриации в Израиль, за ней я, студент Рижского Мединститута Пинхус Хнох (теперь Пинхас Ханох, врач -пенсионер, Израиль), около меня мой друг Павел (Песах) Сегаль,(видна только часть головы) брат известного Вам Соломса Сегалса), он умер в Израиле; далее первая справа Циля Димант, живет в Германии, а рядом с ней-друг моего брата Гриша Эльцефон , двинчанин (Цви Эльцафон, психиатр в Израиле )</div>

<div class="underText"><span style="font-size: 18px;">Fotokopijā ārskats, vasara. No kreisās puses Leibls Hnohs ( tagad Arie Hanoh ), otrā meitene no kreisās puses - Raja Zjomina, nomira pēc izsūtīšanas no Izraelas, blakus Rīgas medicīnas institūta students Pinhuss Hnohs ( tagad Pinhass Hanohs, penisonēts ārsts, Izraēla ), blakus viņam Pāvels Segals ( redzama tikai daļa galvas ), Soloma Segala brālis, viņš nomira Izraēlā. Pirmā no labās puses - Ciļa Dimanta, dzīvo Vācijā, blakus viņai Griša Elcefons ( psihiatrs Izraēlā ) </span> </div>

