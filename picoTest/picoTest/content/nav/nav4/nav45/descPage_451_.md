---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav45
desc: true
type: textframe
assetName: 4-5-1.jpg
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Этот снимок можно назвать Четыре поколения или Четыре поколения младенца. ( А кто этот полугодовой младенец ? Я -Пинцинке (так меня звали все в мои счастливые предвоенные годы). Снимок сделан осенью 1935 года в связи с отъездом семьи маминого брата Давида в Палестину (теперь это Израиль)Первый ряд: Поля Шварц,жена маминого брата Давида и Вуля (Зеэв Шарон) , ее сын, мой кузен, инженер, пенсионер, Израиль. В кресле- моя бабушка Рохе-Эйда Хнох, папина мама, со мной на коленях, рядом- "маленький дедушка" (он был маленького роста ) Мойше Шварц, дед моей мамы, затем-мой папа Гирш (Хиршл) Хнох, крайние- моя тетя Эмма, жена Ицхака, маминого брата и их дочь Сара (живет в Аахене, Германия). Верхний ряд: справа налево. Ицхак Шварц, мамин брат, погиб в рядах Красной армии во Второй мировой войне. Рядом с ним моя мама Эстер Хнох, до брака-Шварц (мои мама и папа умерли в престарелом возрасте в Израиле). Следующая- тетя Люба Хнох, после брака-Виленски, папина сестра, умерла в Израиле, рядом Рохале (Рахель Кремер), погибла в Рижском гетто (?) в 1941 (?). Дальше-Абе-Довид Димант, мамин кузен, умер в Риге после ВМВ, тетя Лиза, женадяди Шлеймы, дядя Шлейме Хнох, папин брат и крайний- дядя Давид Шварц , мамин брат, эмигрировал в Палестину в 1935 и умер там от туберкулеза</div>

<div class="underText"><span style="font-size: 18px;">Fotogrāfēts 1935. gada rudenī sakarā ar Dāvida Švarca ģimenes aizbraukšanu uz Palestīnu ( tagad Izraēlu ). Pirmajā rindā: Poļja Švarca, viņas dēls Vuļa ( Zaev Šaron ( pensionēts inženieris)), Krēslā Rohe Eida Hnoha, pa vidu vecais kungs ar bārdu - Moiše Švbarcs, tad GiršsHnohs, Emma Ichaka un viņas meita Sāra ( dzīvo  henē, Vācijā ). Otrā rinda no labās uz kreiso pusi - Ichaks Švarcs ( miris Otrā pasaules kara laikā ), blakus Estere Hnoha, Ļuba Hnoha, Rahele Kremer ( gājusi bojā Rīgas geto 1941.g. ? ) , Abe Dovid Dimant, Liza Hnoha, Šleime Hnohs, Dāvids Švarcs </span> </div>

