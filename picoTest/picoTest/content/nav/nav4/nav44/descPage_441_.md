---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav44
desc: true
type: textframe
assetName: 4-4-1.jpg
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">1939.g. no 17. jūnija līdz 13. jūlijam V.E. bīskaps J. Rancāns vizitācijas braucienos viesojās 17 Latgales draudzēs, to starpā viņš pabija arī Varakļānos, kur viesus sagaidīja pilsētas galva Pūpols un rabīns Grockis, kurš viesi uzrunāja senebreju valodā. Senebreju valodu zinošais V.E. bīskaps J.Rancāns uz to tāpat arī atbildēja. Vēlāk vietējie ebreji no rabīna centās uzzināt, ko abas augstās personas runāja. Uz to rabins Grockis atbildē∆is, ka to, ko teicis biskaps Rancāns, viņš sapratis ļoti labi, bet ko atbildējis viņš pats - to gan viņš nevienam neteikšot.</div>
<div class="underText"></div>

