---
Title: Uzmetuma nosaukums
SubTitle: Navigācijas ieraksta nosaukums
Description: Navigation item
PageType: descPage01.twig
Parrent: nav111
desc: true
type: textframe
assetName: skice-1-20.png
orderNr: 1
---

<div class="way">Ekrāna nosaukums</div>
<div class="header" style="color:#fff;">Lapas nosaukums</div>

<div class="cardName">Lapas apraksts</div>
<div class="underText">Paraksts</div>

