---
Title: EBREJI VARAKĻĀNIEŠU UN APKĀRTNES IEDZĪVOTĀJU KULTŪRATMIŅĀ
Description: Pico is a stupidly simple, blazing fast, flat file CMS.
PageType: mainPage.twig
---
<div>
    <p>
    Pēc 1935. gada tautas skaitīšanas datiem Varakļānos bija 952 ebreji jeb 57,32% no visu iedzīvotāju kopskaita (Salnītis, Skujenieks, 1936, 530) un viņiem bija noteicošā loma pilsētas uzņēmējdarbībā. Tā uz 1928. gadu, apmēram 90% no kopējā pilsētas tirdzniecības sektora, piederēja ebreju tautības pārstāvjiem (Muzejs Ebreji Latvijā, 1990, f. 1994., pl. B-780, 1). Ebreji no vietējiem iedzīvotājiem atšķīrās gan ar savu kultūru, gan reliģiskajiem priekšstatiem. Nereti šīs atšķirības izraisīja interesantas dzīves situācijas ebreju un vietējo iedzīvotāju savstarpējās attiecībās. Varakļāniešu atmiņas ļāva ielūkoties šajās dažādajās dzīves situācijās
</p>
</div>

