<?php // @codingStandardsIgnoreFile
/**
 * This file is part of Pico. It's copyrighted by the contributors recorded
 * in the version control history of the file, available from the following
 * original location:
 *
 * <https://github.com/picocms/pico-composer/blob/master/index.php>
 *
 * SPDX-License-Identifier: MIT
 * License-Filename: LICENSE
 */



function changeStringInFile($pageType, $dest, $id, $fromString, $toString, $replaceOne = true){
    
    if($pageType == "text" || $pageType == "textframe" || $pageType == "frame"){
        $sourceFile = fopen("content/".$dest."/descpage_".$id."_.md", "r") or die("Unable to open file!");
        $destFile = fopen("content/".$dest."/descpage_".$id."_copy.md", "w+") or die("Unable to open file!");

        $doReplace = true;

        if ($sourceFile and $destFile) {
            while (($line = fgets($sourceFile)) !== false) {
                
                if($doReplace){
                    // read line by line and search for special mockValues
                    $pos = strpos($line, $fromString);
                    
                    if ($pos !== false) {
                        $line = str_replace($fromString, $toString, $line);
                        if($replaceOne == true) $doReplace = false;
                    }
                }

                // add the handled template line to destination file (let's try to keep with low line count)
                fwrite($destFile, $line);
            }

            fclose($sourceFile);
            fclose($destFile);
            
            rename("content/".$dest."/descpage_".$id."_copy.md", "content/".$dest."/descpage_".$id."_.md");
        } else {
            // error opening files.
        }
    } else if($pageType == "nav"){
        $sourceFile = fopen("content/".$dest."/nav_".$id."_.md", "r") or die("Unable to open file!");
        $destFile = fopen("content/".$dest."/nav_".$id."_copy.md", "w+") or die("Unable to open file!");

        $doReplace = true;

        if ($sourceFile and $destFile) {
            while (($line = fgets($sourceFile)) !== false) {
                
                if($doReplace){
                    // read line by line and search for special mockValues
                    $pos = strpos($line, $fromString);
                    
                    if ($pos !== false) {
                        $line = str_replace($fromString, $toString, $line);
                        if($replaceOne == true) $doReplace = false;
                    }
                }

                // add the handled template line to destination file (let's try to keep with low line count)
                fwrite($destFile, $line);
            }

            fclose($sourceFile);
            fclose($destFile);

            rename("content/".$dest."/nav_".$id."_copy.md", "content/".$dest."/nav_".$id."_.md");
        } else {
            // error opening files.
        }
    }
}

function changeLineInFile($fullFilePath, $lineStart, $lineTarget){
    $sourceFile = fopen($fullFilePath, "r") or die("Unable to open file!");
    $destFile = fopen($fullFilePath.".copy", "w+") or die("Unable to open file!");

    $doReplace = true;

    if ($sourceFile and $destFile) {
        while (($line = fgets($sourceFile)) !== false) {

            if($doReplace){
                // read line by line and search for special mockValues
                $pos = strpos($line, $lineStart);

                if ($pos !== false) {
                    $line = str_replace($line, $lineTarget, $line);
                    $doReplace = false;
                }
            }

            // add the handled template line to destination file (let's try to keep with low line count)
            fwrite($destFile, $line);
        }

        fclose($sourceFile);
        fclose($destFile);

        rename($fullFilePath.".copy", $fullFilePath);
    } else {
        // error opening files.
    }
}

function reorderFilesByID($pageType, $fullDestination){
    $pageAmount = 0;

    $pagePrefix = ($pageType == "nav") ? "nav_" : "descPage_"; // could also be index
    if($pageType != "index"){
        $files = glob($fullDestination."/".$pagePrefix."*");
        if ($files){
            $pageAmount = count($files);

            for ($i = 1; $i <= $pageAmount; $i++) {
                // iterates through pageType files and changes order to for loops iterator
                changeLineInFile($files[$i-1], "orderNr:", "orderNr: ".$i);
            }
        }
    }
}

function editItem($pageType, $dest, $id, $orderNr, $hasChild, $assetName, $header, $way, $cardName, $underText){
    echo "page_edited";
    $destSplitted = explode("/",$dest);
    $parrent = end($destSplitted);
    //echo "successfully called editItem <br>";
    
    switch ($pageType) {
        case "text":
            $sourceFile = fopen("content/descPages/descPage_text_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/descPage_".$id."_.md", "w+") or die("Unable to open file!");
            break;
        case "textframe":
            $sourceFile = fopen("content/descPages/descPage_textframe_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/descPage_".$id."_.md", "w+") or die("Unable to open file!");
            break;
        case "frame":
            $sourceFile = fopen("content/descPages/descPage_frame_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/descPage_".$id."_.md", "w+") or die("Unable to open file!");
            break;
        case "gallery":
            $sourceFile = fopen("content/descPages/descPage_frame_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/descPage_".$id."_.md", "w+") or die("Unable to open file!");
            break;
        case "videos":
            $sourceFile = fopen("content/descPages/descPage_frame_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/descPage_".$id."_.md", "w+") or die("Unable to open file!");
            break;
        case "nav":
            $sourceFile = fopen("content/descPages/nav_id_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav_".$id."_.md", "w+") or die("Unable to open file!");
            break;
    }
    
    if ($sourceFile and $destFile) {
        while (($line = fgets($sourceFile)) !== false) {
            
        //echo $line." <br>";
            // read line by line and search for special mockValues
            $pos = strpos($line, "####");
            
            if ($pos !== false) {
                $mockValue = substr($line, $pos, 16); //16 is the char length of all mockValue strings

                switch ($mockValue) {
                    case "####title0000###":
                        $line = str_replace($mockValue, 'user created value', $line);
                        break;
                    case "####parrent00###":
                        $line = str_replace($mockValue, $parrent, $line);
                        break;
                    case "####type00000###":
                         $line = str_replace($mockValue, $pageType, $line);
                         break;
                    case "####assetName###":
                        $line = str_replace($mockValue, $assetName, $line);
                        break;
                    case "####true00000###":
                        $line = str_replace($mockValue, $hasChild, $line);
                        break;
                    case "####orderNr00###":
                        $line = str_replace($mockValue, $orderNr, $line);
                        break;
                    case "####header000###":
                        $line = str_replace($mockValue, $header, $line);
                        break;
                    case "####way000000###":
                        $line = str_replace($mockValue, $way, $line);
                        break;
                    case "####cardName0###":
                        $line = str_replace($mockValue, $cardName, $line);
                        break;
                    case "####underText###":
                        $line = str_replace($mockValue, $underText, $line);
                        break;
                }
            }

            // add the handled template line to destination file (let's try to keep with low line count)
            fwrite($destFile, $line);
        }

        fclose($sourceFile);
        fclose($destFile);
    } else {
        // error opening files.
    } 
    
}

// TODO: does pageAmount work correctly - it shows only for current page sibling amount, not creatable children sibling amount
// TODO: rename $id to $currentId
function createItem($pageType, $dest, $id, $assetName, $childType = "nav.twig") {
    echo "page_edited";
    $destSplitted = explode("/",$dest);
    $parrent = "nav".$id;
    $newPageId = $id."1";
    $pageAmount = 1;
    
    $pagePrefix = ($pageType == "nav") ? "nav_" : "descPage_"; // could also be index
    if($pageType != "index"){
        $files = glob("content/".$dest."/nav".$id."/".$pagePrefix."*");
        if ($files){
            $pageAmount = count($files)+1;

            for ($i = 1; $i <= $pageAmount; $i++) {
                if(!glob("content/".$dest."/nav".$id."/".$pagePrefix.$id.$i."_.md")){
                    $newPageId = "".$id.$i;
                    break;
                }
            }
        }
    }
    
    
    switch ($pageType) {
        case "text":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage00.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }

            $sourceFile = fopen("content/descPages/descPage_text_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "textframe":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage01.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }

            $sourceFile = fopen("content/descPages/descPage_textframe_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "frame":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage02.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }
            
            $sourceFile = fopen("content/descPages/descPage_frame_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "gallery":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage02.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }
            
            $sourceFile = fopen("content/descPages/descPage_gallery_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "pdf":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage03.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }
            
            $sourceFile = fopen("content/descPages/descPage_justframe_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "videos":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "descPage04.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/nav*")){
                echo "descPage not applicable <br />";
            }
            
            $sourceFile = fopen("content/descPages/descPage_justframe_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/descPage_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "nav":
            if (!file_exists("content/".$dest."/nav".$id."/index.md")) {
                createItem("index", $dest, $id, $assetName, "nav.twig");
            }
            else if (glob("content/".$dest."/nav".$id."/desc*")){
                echo "navPage not applicable <br />";
            }
            
            $sourceFile = fopen("content/descPages/nav_id_.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/nav_".$newPageId."_.md", "w+") or die("Unable to open file!");
            break;
        case "index":
            if (!file_exists("content/".$dest."/nav".$id)) {
                mkdir("content/".$dest."/nav".$id, 0777, true);
            }
            
            //echo "<br /> creating index.md <br />".file_exists("content/".$dest."/nav".$id."/index.md")."<br />";
            
            $sourceFile = fopen("content/descPages/index.md", "r") or die("Unable to open file!");
            $destFile = fopen("content/".$dest."/nav".$id."/index.md", "w+") or die("Unable to open file!");

            $pageType = $childType;
            break;
    }
    
    
    if ($sourceFile and $destFile) {
        while (($line = fgets($sourceFile)) !== false) {
            
            // read line by line and search for special mockValues
            $pos = strpos($line, "####");
            
            if ($pos !== false) {
                $mockValue = substr($line, $pos, 16); //16 is the char length of all mockValue strings

                switch ($mockValue) {
                    case "####title0000###":
                        $line = str_replace($mockValue, "Uzmetuma nosaukums", $line);
                        break;
                    case "####parrent00###":
                        $line = str_replace($mockValue, $parrent, $line);
                        break;
                    case "####type00000###":
                        $line = str_replace($mockValue, $pageType, $line);
                        break;
                    case "####assetName###":
                        $line = str_replace($mockValue, $assetName, $line);
                        break;
                    case "####true00000###":
                        $line = str_replace($mockValue, "false", $line);
                        break;
                    case "####orderNr00###":
                        $line = str_replace($mockValue, $pageAmount, $line);
                        break;
                    case "####header000###":
                        $line = str_replace($mockValue, "Lapas nosaukums", $line);
                        break;
                    case "####way000000###":
                        $line = str_replace($mockValue, "Ekrāna nosaukums", $line);
                        break;
                    case "####cardName0###":
                        $line = str_replace($mockValue, "Lapas apraksts", $line);
                        break;
                    case "####underText###":
                        $line = str_replace($mockValue, "Paraksts", $line);
                        break;
                }
            }

            // add the handled template line to destination file (let's try to keep with low line count)
            fwrite($destFile, $line);
        }

        fclose($sourceFile);
        fclose($destFile);
    } else {
        // error opening files.
    }

    //echo "File created. <br/>";
}

///////---- ADDING HTTP REQUEST LOGIC ----\\

// processing POST call for "submit"
if(isset($_POST['addPage'])){ 
    
//    echo $_POST['pageTitle'];
//    echo $_POST['srcName'];
//    echo $_POST['content'];
	$pageType_ = $_POST['pageType'];
	$destination_ = $_POST['destination'];
	$pageId_ = $_POST['pageId'];
	$assetName_ = (isset($_POST['assetName']) ? $_POST['assetName'] : "none");

    
//    echo "admin: ".$_GET['admin']."<br/>";
//    createItem("textframe", "nav/nav1", "14", "Warkland|Vēsture", $_POST['pageTitle'], $_POST['srcName']);
    createItem($pageType_, $destination_, $pageId_, $assetName_);
    
}elseif(isset($_POST['deleteItem'])){ 

// delete nav item and descpage with the id    
    
	$destination_ = $_POST['destination'];
	$pageId_ = $_POST['pageId'];
	$justThis_ = (isset($_POST['justThis']) ? $_POST['justThis'] : "none");

        if($justThis_ == "true") unlink("content/".$destination_."/nav_".$pageId_."_.md");
        else {
            $files = glob("content/".$destination_."/nav".$pageId_."/*");
            if ($files){
                foreach ($files as &$filename) {
                    unlink($filename);
                }
            }

            //unlink("content/".$destination_."/nav".$pageId_);

            rmdir("content/".$destination_."/nav".$pageId_);   
        }
	
}elseif(isset($_POST['editPage'])){ 

    if(isset($_FILES['assetImage']) && $_FILES['assetImage']['size'] > 0){
        $target_dir = "assets/";
        $target_file = $target_dir . basename($_FILES["assetImage"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $check = getimagesize($_FILES["assetImage"]["tmp_name"]);
        if($check !== false) {
            // echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }

        // Check if file already exists
        if (file_exists($target_file)) {
    //        echo "Sorry, file already exists.";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["assetImage"]["size"] > (1024 * 1024 * 10)) {
    //        echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
    //        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["assetImage"]["tmp_name"], $target_file)) {
    //            echo "The file ". basename( $_FILES["assetImage"]["name"]). " has been uploaded.";
            } else {
    //            echo "Sorry, there was an error uploading your file.";
            }
        }
    }
    
    
    $mType_ = $_POST['mType'];
    $destination_ = $_POST['destination'];
    $pageId_ = $_POST['pageId'];
    $mOrderNr_ = $_POST['mOrderNr'];
    $mSwitch_ = $_POST['mOrderSwitch'];
    $mHasChild_ = $_POST['mHasChild'];
    $mAssetName_ = $_POST['mAssetName'];
    $mHeader_ = (isset($_POST['mHeader']) ? $_POST['mHeader'] : "none");
    $mWay_ = (isset($_POST['mWay']) ? $_POST['mWay'] : "none");
    $mCardName_ = $_POST['mCardName'];
    $mUnderText_ = (isset($_POST['mUnderText']) ? $_POST['mUnderText'] : "none");
    
//    echo "editItem(pageType, dest, id, orderNr, hasChild, assetName, header, way, cardName, underText)";
//    echo "editItem('".$mType_."', '".$destination_."', '".$pageId_."', '".$mOrderNr_."', '".$mHasChild_."', '".$mAssetName_."', '".$mHeader_."', '".$mWay_."', '".$mCardName_."', '".$mUnderText_."')";
// function editItem($pageType, $dest, $id, $orderNr, $hasChild, $assetName, $header, $way, $cardName, $underText){

//    echo "viva la death<br>";
    
    editItem($mType_, $destination_, $pageId_, $mOrderNr_, $mHasChild_, $mAssetName_, $mHeader_, $mWay_, $mCardName_, $mUnderText_);
    
    if($mSwitch_ != "none"){
        $orderDetails = explode("|", $mSwitch_);
        $currentNr = $orderDetails[0];
        $targetNr = $orderDetails[1];
        $targetId = $orderDetails[2];

        changeStringInFile($mType_, $destination_, $pageId_, "orderNr: ".$currentNr, "orderNr: ".$targetNr);
        changeStringInFile($mType_, $destination_, $targetId, "orderNr: ".$targetNr, "orderNr: ".$currentNr);
    }
    
}else{
//    echo '%base_url%?descriptionPage00';
    //code to be executed  
}

// load dependencies
// pico-composer MUST be installed as root package
if (is_file(__DIR__ . '/vendor/autoload.php')) {
    require_once(__DIR__ . '/vendor/autoload.php');
} else {
    die("Cannot find 'vendor/autoload.php'. Run `composer install`.");
}

// instance Pico
$pico = new Pico(
    __DIR__,    // root dir
    'config/',  // config dir
    'plugins/', // plugins dir
    'themes/'   // themes dir
);

// override configuration?
//$pico->setConfig(array());

// run application
echo $pico->run();
